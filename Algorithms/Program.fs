﻿// 在 http://fsharp.net 上了解有关 F# 的更多信息
// 请参阅“F# 教程”项目以获取更多帮助。

open System
open System.Collections
open Microsoft.FSharp.Collections
#time


let memo = Array.zeroCreate<int>(1000)

let rec fib n = 
    if n <= 1 then 1
    else fib (n-1) + fib (n-2)

let rec fib_optimize n = 
    if n <= 1 then 1
    elif memo.[n] <> 0 then memo.[n]
    else memo.[n] <- fib (n-1) + fib (n-2)
         memo.[n]

let que = new Queue()


fib 42

fib_optimize 42

[4;5;6;1] |> List.sort |> List.rev 

List.fold

List.map2 (fun x y -> x*y) [1;2;3;4] [5;6;7;8] |> List.reduce (+)

List.map2 (fun x y -> x*y) [1;2;3;4] [5;6;7;8] |> List.fold (+) 0


let n = 4
let W = 5
let w = [|2;1;3;2|]
let v = [|3;2;4;2|]
let dp = Array2D.init 5 6 (fun x y -> -1)

let rec rsolve i j =
    if dp.[i, j] >= 0 then
        dp.[i, j]
    else
        if i = n then
            0
        elif j < w.[i] then
            dp.[i, j] <- (rsolve (i+1) j)
            dp.[i,j]
        else
            dp.[i, j] <- max (rsolve (i+1) j) ((rsolve (i+1) (j-w.[i]))+v.[i])
            dp.[i,j]

rsolve 0 W
dp


//背包问题递推解决办法
//从前i种物品开始挑选出物品
let dp = Array2D.init 5 6 (fun x y -> 0)
let solve() = 
    for i = n-1 downto 0 do
        for j = 0 to W do 
            if j < w.[i] then
                dp.[i,j] <- dp.[i+1,j]
            else
                dp.[i,j] <- max (dp.[i+1, j]) (dp.[i+1, j-w.[i]]+v.[i])
solve()
dp

let n = 3
let W = 7
let w = [|3;4;2|]
let v = [|4;5;3|]
let dp = Array2D.init 4 8 (fun x y -> 0)
let solvect() = 
    for i = n-1 downto 0 do
        for j = 0 to W do 
            for ct = 0 to j/w.[i] do
                dp.[i,j] <- max (dp.[i, j]) (dp.[i+1, j-w.[i]*ct]+v.[i]*ct)

solvect()


let n = 3
let W = 7
let w = [|3;4;2|]
let v = [|4;5;3|]
let dp = Array2D.init 4 8 (fun x y -> 0)
let solvect2() = 
    for i = n-1 downto 0 do
        for j = 0 to W do 
            if j < w.[i] then
                dp.[i,j] <- dp.[i+1,j]
            else
                dp.[i,j] <- max (dp.[i+1, j]) (dp.[i, j-w.[i]]+v.[i])

solvect2()

dp.[0,7]


open System
//
let n = 4
let W = 5u
let w = [|2u;1u;3u;2u|]
let v = [|3;2;4;2|]
let V = Array.max v

let dp = Array2D.init (n+1) (n*V+1) (fun x y -> 0u)

dp.[0,*] <- Array.init (n*V+1) (fun x -> UInt32.MaxValue)
dp.[0,0] <- 0u

let solve() = 
    for i = 0 to n-1 do
        for j = 0 to n*V do 
            if j < v.[i] then
                dp.[i+1,j] <- dp.[i,j]
            else
                dp.[i+1,j] <- min (dp.[i, j]) (dp.[i, j-v.[i]]+ w.[i])

solve()
dp
dp.[n,*] |> Array.rev |> Array.find (fun x -> x <= W)



//最小子序列问题
let s = "abcd"
let t = "becd"
let dp = Array2D.init 6 6 (fun x y -> 0)
let solve() = 
    for i = 0 to s.Length-1 do
        for j = 0 to t.Length-1 do
            if s.[i] = t.[j] then
                dp.[i+1,j+1] <- dp.[i,j]+1
            else
                dp.[i+1,j+1] <- max dp.[i+1,j] dp.[i,j+1]

solve()
dp
dp.[4, 4]



for i = 0 to 10 do printf "%d" i


let dp = [|for i= 10 downto 0 do yield i|]


let dp = [|for i in 1 .. 10 -> i|]






[<EntryPoint>]
let main argv = 
    printfn "%A" argv
    0 // 返回整数退出代码
