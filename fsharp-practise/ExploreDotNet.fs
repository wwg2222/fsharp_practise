﻿module ExploreDotNet

System.IO.Directory.SetCurrentDirectory(__SOURCE_DIRECTORY__)

//System.IO.Directory.GetCurrentDirectory()

open System

[<CLIMutable>]
type DateSerTest = 
    {
        Local:DateTime;
        Utc:DateTime
    }

let ser = new System.Xml.Serialization.XmlSerializer(typeof<DateSerTest>)

let d = {
    Local = DateTime.SpecifyKind(new DateTime(2014, 10, 1), DateTimeKind.Local)
    Utc = DateTime.SpecifyKind(new DateTime(2014, 10, 1), DateTimeKind.Utc)
}

let testSerialization(dt:DateSerTest) =
    let filename = "serialization.xml"
    use ms = new IO.MemoryStream()
    ser.Serialize(ms, o = dt)
    Text.Encoding.Default.GetString(ms.GetBuffer()) |> Console.WriteLine
    ms.Close()

testSerialization d

Environment.GetEnvironmentVariable "paTH"

Environment.GetEnvironmentVariable "ie_home"

open Microsoft.FSharp.Quotations

let express:Expr<int> = <@ 1 + 1 @>
let expressraw = <@@ 1 + 1 @@>

<@ 1 + %%expressraw @>
<@ 1 + %express @>

<@@ 1 + %%expressraw @@>
<@@ 1 + %express @@>


