﻿module ListViewWinform

open System
open System.Data
open System.Drawing
open System.Windows.Forms

type Product = {
    Name:string;
    Num1:int;
    Num2:int;
}

let AddProductItem ():Control array = 
    let subpanel = new TableLayoutPanel()

    let tb1 = new Label()
    tb1.Text <- "零件名称"
    tb1.Width <- 150
    let tb2 = new Label()
    tb2.Text <- "应收数量"
    let tb3 = new Label()
    tb3.Text <- "实收数量"
    let tb4 = new Label()
    tb4.Text <- "货架编号"
    let btn = new Button()
    btn.Text <- "零件明细"
    btn.Click.Add(fun _ -> 
        tb2.Text <- "应收数量" + DateTime.Now.Second.ToString())

    //table profile
    subpanel.ColumnCount <- 2
    subpanel.ColumnStyles.Add(ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.f)) |> ignore
    subpanel.ColumnStyles.Add(ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.f)) |> ignore
    subpanel.RowCount <- 3
    subpanel.RowStyles.Add(RowStyle(System.Windows.Forms.SizeType.Percent, 33.f)) |> ignore
    subpanel.RowStyles.Add(RowStyle(System.Windows.Forms.SizeType.Percent, 33.f)) |> ignore
    subpanel.RowStyles.Add(RowStyle(System.Windows.Forms.SizeType.Percent, 34.f)) |> ignore

    subpanel.Controls.Add(tb1, 0, 0)
    subpanel.SetColumnSpan(tb1, 2)
    subpanel.Controls.Add(tb2, 1, 0)
    subpanel.Controls.Add(tb3, 1, 1)
    subpanel.Controls.Add(tb4, 2, 0)
    subpanel.Controls.Add(btn, 2, 1)
    let splitline = new Label()
    splitline.Text <- "______________________________"
    splitline.AutoSize <- true
    [|subpanel;splitline|]


let form = new Form(Width = 300, Height = 300, Visible = true, Text = "Hello GUIWinform")
form.AutoSize <- false
form.TopMost <- true
//form.SuspendLayout()

let panel = new FlowLayoutPanel()

form.Controls.Add(panel)
panel.Location <- new Point(30, 10)
panel.FlowDirection <- FlowDirection.TopDown
panel.WrapContents <- false
panel.Dock <- DockStyle.Fill
panel.Width <- 250
panel.Height <- 200
panel.BorderStyle <- BorderStyle.FixedSingle
panel.AutoScroll <- true
form.Controls.Add(panel)
form.Show()

//panel.SuspendLayout()
panel.Controls.AddRange(AddProductItem())
panel.Controls.AddRange(AddProductItem())
panel.Controls.AddRange(AddProductItem())
panel.Controls.AddRange(AddProductItem())

panel.Controls.Clear()




//let productlist = [{Name="paart 1"; Num1 = 1234; Num2 = 87897};{Name = "part 2"; Num1 = 4123;Num2 = 234}]



form.Show()
