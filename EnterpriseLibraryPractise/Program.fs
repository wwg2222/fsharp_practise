﻿// 在 http://fsharp.net 上了解有关 F# 的更多信息
// 请参阅“F# 教程”项目以获取更多帮助。
#if INTERACTIVE
#r "E:/WorkHell/fsharp-practise/packages/EnterpriseLibrary.Data.6.0.1304.0/lib/NET45/Microsoft.Practices.EnterpriseLibrary.Data.dll"
#r "E:/WorkHell/fsharp-practise/packages/EnterpriseLibrary.Common.6.0.1304.0/lib/NET45/Microsoft.Practices.EnterpriseLibrary.Common.dll"
#r "System"
#r "System.Data"
#r "System.Configuration"
#endif




open System
open System.Data
open System.Data.Common
open System.Data.SqlClient
open Microsoft.Practices.EnterpriseLibrary.Data
open Microsoft.Practices.EnterpriseLibrary.Common.Configuration
open Microsoft.Practices.EnterpriseLibrary.Data.Sql
open Microsoft.Practices.EnterpriseLibrary.Data.Configuration
open System.Threading
open System.Threading.Tasks
open System.Configuration

let DisplayRowValue (reader:IDataReader) = 
    while reader.Read() do
        for i = 0 to reader.FieldCount-1 do
            Console.WriteLine("{0}={1}", reader.GetName(i), reader.[i].ToString())
    Console.WriteLine()

type Product() = 
    member val ID:int=0 with get, set
    member val Name:string="" with get, set
    member val Description:string="" with get, set

 

[<EntryPoint>]
let main argv = 
#if COMPILED
    let factory = new DatabaseProviderFactory()              
#else
    let path = __SOURCE_DIRECTORY__ + "\FsiApp.config"
    let fileMap = ConfigurationFileMap(path)
    let config = ConfigurationManager.OpenMappedMachineConfiguration(fileMap)
    let factory = new DatabaseProviderFactory(fun s -> config.GetSection(s))
#endif
    let defaultDB = factory.CreateDefault()
    let reader = defaultDB.ExecuteReader(CommandType.Text, "select top 1 * from orderlist")
    DisplayRowValue reader

    let reader = defaultDB.ExecuteReader("ListOrdersByState", [|box "Colorado"|])//box var type object
    DisplayRowValue reader
    using(defaultDB.ExecuteReader("GetProductList", [|box "%bike%"|]))(fun reader ->
        DisplayRowValue reader)

    
    let sqlStatement = "select top 1 * from OrderList where State like @state"
    using(defaultDB.GetSqlStringCommand(sqlStatement))(fun sqlCmd ->
        defaultDB.AddInParameter(sqlCmd, "state", DbType.String, "New York")
        using(defaultDB.ExecuteReader(sqlCmd))(fun sqlReader ->
            DisplayRowValue sqlReader))

    using(defaultDB.GetStoredProcCommand("ListOrdersByState"))(fun sprocCmd ->
        defaultDB.AddInParameter(sprocCmd, "state", DbType.String, "New York")
        using(defaultDB.ExecuteReader(sprocCmd))(fun sprocReader ->
            DisplayRowValue sprocReader))
    
    let productData = defaultDB.ExecuteSprocAccessor<Product>("GetProductList", [|box "%bike%"|])
    let result = query{
                        for productItem in productData do
                        where (productItem.Description <> null)
                        sortBy productItem.Name
                        select productItem
    }
    result |> Seq.iter (fun i -> printfn "Product Name:%A\nDescription:%A\n" i.Name i.Description)


//by automatic generate accessor
    let sqlproduct = defaultDB.ExecuteSqlStringAccessor<Product>("select * from Products")
    query{
        for productItem in sqlproduct do
        where (productItem.Description <> null)
        sortBy productItem.Name
        select productItem
    } |> Seq.iter (fun i -> printfn "id:%A\nProduct Name:%A\nDescription:%A\n" i.ID i.Name i.Description)
    

//// no xml
//        sqlserverDB.AddInParameter(xmlCmd, "state", DbType.String, "Colorado")
//        using(sqlserverDB.ExecuteReader(xmlCmd)) (fun sqlReader ->
//            DisplayRowValue sqlReader))

    let sqlserverDB = factory.Create("ExampleDatabase") :?> SqlDatabase 
    using(sqlserverDB.GetSqlStringCommand("select * from OrderList where State = @state for xml auto"))(fun xmlCmd->
        xmlCmd.Parameters.Add(new SqlParameter("state", "Colorado")) |> ignore
        using(sqlserverDB.ExecuteXmlReader(xmlCmd))(fun reader ->
            while not reader.EOF do
                if reader.IsStartElement() then
                    Console.WriteLine(reader.ReadOuterXml())))

    using(defaultDB.GetSqlStringCommand("select [Name] from States"))(fun sqlCmd ->
        using(defaultDB.ExecuteReader(sqlCmd))(fun sqlReader ->
            DisplayRowValue sqlReader))

    using(defaultDB.GetSqlStringCommand("select [Name] from States"))(fun sqlCmd ->
        printfn "%A" (defaultDB.ExecuteScalar(sqlCmd).ToString()))

    using(defaultDB.GetSqlStringCommand("select [Name] from States"))(fun sqlCmd ->
        defaultDB.ExecuteScalar(sqlCmd).ToString())


    let asyncDB = factory.Create("AsyncExampleDatabase")
    printfn "%A" (asyncDB.SupportsAsync)
    printfn "%A" (sqlserverDB.SupportsAsync)
    printfn "%A" (defaultDB.SupportsAsync)

    let AsyncGetDBInfo (db:Database) = 
        using(new ManualResetEvent(false))(fun doneWaitingEvent -> 
            using (new ManualResetEvent(false))(fun readCompleteEvent->
                try
                    let cmd = db.GetStoredProcCommand("ListOrdersSlowly")
                    db.AddInParameter(cmd, "state", DbType.String, "Colorado")
                    db.AddInParameter(cmd, "status", DbType.String, "DRAFT")
                    db.BeginExecuteReader(cmd, (fun asyncResult ->
                        doneWaitingEvent.Set() |> ignore 
                        try
                            try
                                using(db.EndExecuteReader(asyncResult))(fun reader ->
                                    DisplayRowValue(reader)
                                )
                            with
                            | ex -> printfn "Error afer data access completed: %A" ex.Message
                        finally
                            readCompleteEvent.Set() |> ignore
                    
                    ), null) |> ignore
                    while not (doneWaitingEvent.WaitOne(1000)) do
                        Console.Write "Waiting ..."

                    readCompleteEvent.WaitOne() |> ignore
                with
                | ex -> printfn "Error while starting data access :%A" ex.Message
        ))

    AsyncGetDBInfo asyncDB
//    AsyncGetDBInfo defaultDB


    let DoReadDataAsyncronouslyTask (db:Database) = async{
        try
            let cmd = db.GetStoredProcCommand("ListOrdersSlowly")
            db.AddInParameter(cmd, "state", DbType.String, "Colorado")
            db.AddInParameter(cmd, "status", DbType.String, "DRAFT")
            use timer = new Timer(fun _ -> printf "Waiting...")
            timer.Change(0, 1000) |> ignore
            use! reader = Async.FromBeginEnd(cmd,
                                             (fun (cmd:DbCommand,callback,state) -> db.BeginExecuteReader(cmd, callback, state)),
                                             db.EndExecuteReader) 
            timer.Change(Timeout.Infinite, Timeout.Infinite)|>ignore
            printf "\n\n"
            DisplayRowValue reader
        with
        | ex -> printfn "Error while starting data access: %A" ex.Message
    }


    DoReadDataAsyncronouslyTask(asyncDB) |> Async.RunSynchronously
//    DoReadDataAsyncronouslyTask(defaultDB) |> Async.RunSynchronously

    let DoReadAccessorDataAsyncronouslyTask (db:Database) = async{
        try
            let accessor = db.CreateSprocAccessor<Product>("GetProductsSlowly")
            use timer = new Timer(fun _ -> printf "Waiting...")
            timer.Change(0, 1000) |> ignore
            let! productData = Async.FromBeginEnd((fun (callback,state) -> accessor.BeginExecute(callback, state, [|(box "%bike%");(box 20)|])),
                                             accessor.EndExecute) 
            timer.Change(Timeout.Infinite, Timeout.Infinite)|>ignore
            printfn ""
            //printfn "%A" productData
            let result = query{
                                for productItem in productData do
                                where (productItem.Description <> null)
                                sortBy productItem.Name                   
                                select productItem
            }
            productData |> Seq.iter (fun i -> printfn "Product Name:%A\nDescription:%A\n" i.Name i.Description)
            printf "\n\n"
        with
        | ex -> printfn "Error while starting data access: %A" ex.Message
    }



    DoReadAccessorDataAsyncronouslyTask(asyncDB) |> Async.RunSynchronously

    DisplayRowValue (defaultDB.ExecuteReader(CommandType.Text, "select * from Products where [ID]=84"))
    let oldDescription = "Carries 4 bikes securely; steel construction, fits 2\" receiver hitch."
    let newDescription = "Bikes tend to fall off after a few miles"
    let cmd = defaultDB.GetStoredProcCommand("UpdateProductsTable")
    defaultDB.AddInParameter(cmd, "productID", DbType.Int32, 84)
    defaultDB.AddInParameter(cmd, "description", DbType.String, newDescription)
    defaultDB.ExecuteNonQuery(cmd) |> ignore
    DisplayRowValue (defaultDB.ExecuteReader(CommandType.Text, "select * from Products where [ID]=84"))    
    defaultDB.SetParameterValue(cmd, "description", oldDescription)
    defaultDB.ExecuteNonQuery(cmd) |> ignore
    DisplayRowValue (defaultDB.ExecuteReader(CommandType.Text, "select * from Products where [ID]=84"))    






    0 // 返回整数退出代码

