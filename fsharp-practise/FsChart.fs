﻿module FsChart

#if INTERACTIVE
#r "System.Windows.Forms.DataVisualization.dll"
#r @"E:\WorkHell\fsharp-practise\packages\FSharp.Charting.0.90.6\lib\net40\FSharp.Charting.dll"
#r "FSharp.Compiler.Interactive.Settings.dll"
#endif

open FSharp.Charting
open System
open System.Windows.Forms.VisualStyles
open Microsoft.FSharp.Compiler.Interactive

#if INTERACTIVE
fsi.AddPrinter(fun (ch:FSharp.Charting.ChartTypes.GenericChart) -> ch.ShowChart();"(Chart)")
#endif

Chart.Line [for x in 1.0 .. 100.0 -> (x, x ** 2.0)]


let curvyData = [ for x in 0.0 .. 0.02 .. 2.0 * Math.PI -> (sin x, cos x * sin x)]

curvyData |> Chart.Line

//Part 2
let rnd = new Random()
let rand() = rnd.NextDouble()
let randomPoints = [ for i in 0 .. 1000 -> rand(), rand() ]

Chart.Point randomPoints

//
let highData = [ for x in 1.0 .. 100.0 -> (x, 3000.0 + x ** 2.0) ]

Chart.Line(highData,Name="Rates").WithYAxis(Min=2000.0).WithXAxis(Log=true)
//Part 3

let j = {X = 3; Y = 9}
j.swap()

//bar and column
