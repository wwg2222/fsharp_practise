﻿module SemiLoggingPractise

#if INTERACTIVE
#r "E:/WorkHell/fsharp-practise/packages/EnterpriseLibrary.Logging.6.0.1304.0/lib/NET45/Microsoft.Practices.EnterpriseLibrary.Logging.dll"
#r  @"E:\WorkHell\fsharp-practise\packages\EnterpriseLibrary.SemanticLogging.2.0.1406.1\lib\net45\Microsoft.Practices.EnterpriseLibrary.SemanticLogging.dll" 
#r  @"E:\WorkHell\fsharp-practise\packages\EnterpriseLibrary.SemanticLogging.EventSourceAnalyzer.2.0.1406.1\lib\net45\Microsoft.Practices.EnterpriseLibrary.SemanticLogging.EventSourceAnalyzer.dll"
#r "System"
#r "System.Data"
#r "System.Configuration"
#r "System.Xml.Linq"
#r "System.Data.Linq"
#r  @"E:\WorkHell\fsharp-practise\SemanticAid\bin\Debug\SemanticAid.dll"
#endif


open System
open System.IO
open System.Diagnostics.Tracing
open System.ComponentModel
open System.Data.Linq
open Microsoft.FSharp.Core.LanguagePrimitives
open Microsoft.Practices.EnterpriseLibrary.SemanticLogging
open Microsoft.Practices.EnterpriseLibrary.SemanticLogging.Formatters
open Microsoft.Practices.EnterpriseLibrary.SemanticLogging.Sinks
open Microsoft.Practices.EnterpriseLibrary.SemanticLogging.Utility
open SemanticAid


//log category
//[<EventSource(Name = "MyCompany")>]
//type Keywords with
//    static let Page:EventKeywords = EnumOfValue 1L
//    static let DataBase:EventKeywords = EnumOfValue 2L
//    static let Perf:EventKeywords = EnumOfValue 4L
//    static let Diagnostic:EventKeywords = EnumOfValue 8L

module MKeywords =                                
    [<Literal>]
    let Page:EventKeywords = EnumOfValue 1L
    [<Literal>]
    let DataBase:EventKeywords = EnumOfValue 2L
    [<Literal>]
    let Perf:EventKeywords = EnumOfValue 4L
    [<Literal>]
    let Diagnostic:EventKeywords = EnumOfValue 0x8L

module Tasks =
    [<Literal>]
    let Page:EventTask = enum 1
    [<Literal>]
    let DBQuery:EventTask = enum 2
//let t = new SemanticAid.MyCompanyEventSource

//let inline Keyconvert (x:Keywords) = x |> int64 |> EnumOfValue 
//[<EventSource(Name = "MyCompany")>]                                                                                        
type MyCompanyEventSource with 
    //singleton pattern
    //static let _log = new MyCompanyEventSource()
    //static member Log = _log
    [<EventAttribute(1, Message = "Application Failure: {0}", Level = EventLevel.Critical, Keywords=MKeywords.Page)>]
    member this.Failure (message:string) = 
        this.WriteEvent(1, message)
    
    [<Event(2, Message = "Starting up",Level = EventLevel.Informational)>]
    member this.Startup ()= 
        this.WriteEvent(2)

EventSourceAttribute.

EventSourceAnalyzer.InspectAll(new MyCompanyEventSource())


//    [<Event(3, Message = "loading page {1} activityID = {0}", Opcode = EventOpcode.Start, Task = TaskPage, Level = EventLevel.Informational, Keywords = Page)>]
//    member internal this.PageStart (ID:int) (url:string) = 
//        if this.IsEnabled() then this.WriteEvent(3, ID, url)

let test = new MyCompanyEventSource()
test.Failure("xx")

let newtest = test :> EventSource
newtest.


EventSource.GetName(typeof<MyCompanyEventSource>)
EventSource.GenerateManifest(typeof<MyCompanyEventSource>, "")
List.ofSeq(MyCompanyEventSource.GetSources()).[1]




    Keywords.Page.GetType()

let listener1 = new ObservableEventListener()
listener1.LogToConsole()
listener1.EnableEvents(test, EventLevel.LogAlways,
        Keywords.All)

