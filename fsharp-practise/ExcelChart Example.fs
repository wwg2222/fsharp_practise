﻿module ExcelChart_Example

open ExcelChartLib.ExcelChart

//open ExcelChart

let pi = System.Math.PI

// Simple function plot

let f x = cos x
let p = Plot(f, (0., 1.))
let g x = sin x
p.Add(g)
let h x = f(x) * g(x*x)
p.Add(h)
p.Rescale(-pi, pi)
p.Zoom(200)
    
let i x = f(x) * g(x) * x
p.Add(i)
p.Zoom(500)

// 3D plots on funky functions
let booth x y = 
    pown (x + 2. * y - 7.) 2 
    + pown (2. * x + y - 5.) 2
    
let s1 = Surface(booth, (-10., 10.), (-10., 10.))

let bidfun (a,b,c,Am) x y =
    x*a + y*b + (Am-x-y)*c-1.0*Am

let bidnew = bidfun <| (1.65,3.40,4.45,100.0)

let s = Surface(bidfun <| (1.65, 0.0, 0.0, 100.0), (0.0, 100.0), (0.0, 100.0))
let s1 = Surface(bidfun <| (0.0, 3.4, 0.0, 100.0), (0.0, 100.0), (0.0, 100.0))
let s2 = Surface(bidfun <| (0.0, 0.0, 4.45, 100.0), (0.0, 100.0), (0.0, 100.0))

s.Zoom(100)
s.Rescale
    
let branin x y = 
    pown (y - (5. / (4. * pown pi 2)) * pown x 2 + 5. * x / pi - 6.) 2 
    + 10.* (1. - 1./ (8. * pi)) * cos(x) + 10.
    
let s2 = Surface(branin, (-5., 10.), (0., 15.))
s2.Zoom(50)
    
let schwefel x y = 
    - sin(sqrt(abs(x))) * x 
    - sin(sqrt(abs(y))) * y 
let s3 = Surface(schwefel, (-1., 1.), (-1., 1.))
s3.Zoom(50)
s3.Rescale((-10., 10.), (-10., 10.))
s3.Rescale((-100., 100.), (-100., 100.))
s3.Rescale((-1000., 1000.), (-1000., 1000.))

