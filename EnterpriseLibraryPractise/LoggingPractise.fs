﻿module LoggingPractise

#if COMPILED
#else
#r "E:/WorkHell/fsharp-practise/packages/EnterpriseLibrary.Common.6.0.1304.0/lib/NET45/Microsoft.Practices.EnterpriseLibrary.Common.dll"
#r "E:/WorkHell/fsharp-practise/packages/EnterpriseLibrary.Logging.6.0.1304.0/lib/NET45/Microsoft.Practices.EnterpriseLibrary.Logging.dll"
#r "System"
#r "System.Data"
#r "System.Configuration"
#endif

open System
open System.Diagnostics
open System.Configuration
open Microsoft.Practices.EnterpriseLibrary.Logging
open Microsoft.Practices.EnterpriseLibrary.Logging.Configuration
open Microsoft.Practices.EnterpriseLibrary.Logging.Formatters
open Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners

//make logging block
//config format -> trace listener

let config = new LoggingConfiguration()

let briefFormatter = new TextFormatter("Timestamp:{timestamp(local)}{newline}Message:{message}{newline}")
let flatFileTraceListener = new FlatFileTraceListener(@"e:\workhell\fsharp-practise\flatfile.log", "----------------------------------------", "----------------------------------------", briefFormatter)
config.AddLogSource("DiskFiles", SourceLevels.All, true).AddTraceListener(flatFileTraceListener)
let logWriter = new LogWriter(config)
logWriter.IsLoggingEnabled()

logWriter.Write("Hello Amber")
logWriter.Write("goodbye Amber")
