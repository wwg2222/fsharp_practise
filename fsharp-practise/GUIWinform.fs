﻿module GUIWinform

#if INTERACTIVE
#I @"E:\WorkHell\fsharp-practise"
#r """.\packages\SpreadsheetLight.dll"""
#r """.\packages\SG-6.0.3.151\Bin\SpreadsheetGear.dll"""
#endif

open System.Windows.Forms
open System.Drawing
open System
open SpreadsheetGear
open SpreadsheetLight

let form = new Form(Width = 400, Height = 300, Visible = true, Text = "Hello GUIWinform")
form.TopMost <- true

let ss = new Windows.Forms.WorkbookView()
form.Controls.Add(ss)
ss.Dock <- DockStyle.Fill

let df = Factory.GetWorkbook("""e:\workhell\iolist\interlock\MOCVD test chamber interlock_20131225.xls""")
ss.ActiveWorkbook <- df

//form.Click.Add (fun _ -> 
//                    form.Text <- sprintf "form click at %i" DateTime.Now.Ticks)  
let panel = new FlowLayoutPanel()
form.Controls.Add(panel)
panel.Dock <- DockStyle.Fill
panel.WrapContents <- true

let greenbutton = new Button()
greenbutton.Text <- "Make the background green"
greenbutton.Click.Add (fun _ -> form.BackColor <- Color.LightGreen)
panel.Controls.Add(greenbutton)
greenbutton.AutoSize <- true

let yellowbutton = new Button()
yellowbutton.Text <- "Make Me Yellow"
yellowbutton.Click.Add (fun _ -> form.BackColor <- Color.Yellow)
panel.Controls.Add(yellowbutton)
yellowbutton.AutoSize <- true
yellowbutton.Dock <- DockStyle.Fill
panel.FlowDirection <- FlowDirection.TopDown

form.Show()

open System.Threading

Thread.CurrentThread.ManagedThreadId


