﻿namespace TestLibrary

open NUnit.Framework

[<TestFixture>]
type TestClass() = 

    [<Test>]
    member this.oneplusoneistwo() = 
        Assert.AreEqual(2, 1+1)

