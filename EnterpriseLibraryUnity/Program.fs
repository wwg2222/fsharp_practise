﻿// 在 http://fsharp.net 上了解有关 F# 的更多信息
// 请参阅“F# 教程”项目以获取更多帮助。

#if INTERACTIVE
#r @"E:\WorkHell\fsharp-practise\packages\Unity.3.5.1404.0\lib\net45\Microsoft.Practices.Unity.dll"
#r @"E:\WorkHell\fsharp-practise\packages\Unity.3.5.1404.0\lib\net45\Microsoft.Practices.Unity.Configuration.dll"
#r @"E:\WorkHell\fsharp-practise\packages\Unity.Interception.3.5.1404.0\lib\Net45\Microsoft.Practices.Unity.Interception.dll"
#r @"E:\WorkHell\fsharp-practise\packages\Unity.3.5.1404.0\lib\net45\Microsoft.Practices.Unity.RegistrationByConvention.dll"
#endif
           
open System
open System.Collections
open Microsoft.Practices.Unity
open Microsoft.Practices.Unity.InterceptionExtension

type ITenantStore = 
    abstract member Msg : unit->unit

type IExtend = 
    abstract member Say : unit -> unit

//type TenantStore()=  
//    abstract Msg : unit -> unit
//    default x.Msg() = 
//        printfn "Hello, it's TenantStore"
//    interface ITenantStore with 
//        member x.Msg() = x.Msg()

type testClass() = 
    abstract member Test : unit -> unit
    default x.Test() = 
        printfn "hello "


type TenantStore() as x= 
    //do printfn "new TenantStore %A" (x.GetHashCode())
    abstract Msg : unit -> unit
    default x.Msg() = 
        printfn "Hello, it's TenantStore"
    abstract Dispose : unit -> unit
    default x.Dispose() = 
        printfn "TenantStore hase been cleaned"
    interface ITenantStore with
        member x.Msg() = 
            x.NewMethod()
            x.Msg()
    interface IDisposable with
        member x.Dispose() = 
            x.Dispose()
    member x.NewMethod() = 
        printfn "just test from TenantStore"
    
//version2
//type TenantStore() as x= 
//    //do printfn "new TenantStore %A" (x.GetHashCode())
//    interface ITenantStore with
//        member x.Msg() = 
//            printfn "Hello, it's TenantStore"
//    interface IDisposable with
//        member x.Dispose() = 
//            printfn "TenantStore hase been cleaned"


type ManagementController(st:ITenantStore, _name:string) =
    member this.TStore = st
    member val Name = _name with get, set
    interface IDisposable with
        member this.Dispose() = printfn "Control %A hase been cleaned" this.Name

type ExManagementController(st:ITenantStore, _name:string) =
    member this.TStore = st
    member val Name = _name with get, set
    interface IDisposable with
        member this.Dispose() = printfn "Control %A hase been cleaned" this.Name

type CombineManagement(m1:ManagementController, m2:ExManagementController) = 
    member this.M1 = m1
    member this.M2 = m2

type IMessage<'a> = 
    abstract member Send : 'a -> unit

type Message<'a>() = 
    interface IMessage<'a> with
        member this.Send(msg:'a) = 
            printfn "Send Message %A" msg


container.RegisterType<ITenantStore, TenantStore>(new PerResolveLifetimeManager())
container.RegisterType<ManagementController>(new InjectionConstructor(typeof<ITenantStore>, "xx"))
container.RegisterType<ExManagementController>(new InjectionConstructor(typeof<ITenantStore>, "yy"))
container.RegisterType<CombineManagement>()


type LogingInterceptionBehavior() = 
    let WriteLog message = 
        printfn "From the logging interceptor: %A" message
    interface IInterceptionBehavior with
        member x.Invoke((input:IMethodInvocation), (getNext:GetNextInterceptionBehaviorDelegate)) = 
            String.Format("Invoke method {0}:{2} at {1}", input.MethodBase, DateTime.Now.ToLongTimeString(), input.MethodBase.DeclaringType) |>  WriteLog
            let result = getNext.Invoke().Invoke(input, getNext)
            match result.Exception with
            | null ->
                String.Format("Method {0}:{3} returned {1} at {2}", input.MethodBase, result.ReturnValue, DateTime.Now.ToLongTimeString(), input.MethodBase.DeclaringType) |> WriteLog
            | _ ->
                String.Format("Method {0} threw exception {1} at {2}", input.MethodBase, result.Exception.Message, DateTime.Now.ToLongTimeString()) |> WriteLog
            result
        member x.GetRequiredInterfaces() =
            Type.EmptyTypes |> Seq.ofArray
        member x.WillExecute with get() = true



//let container = new UnityContainer()
//container.AddNewExtension<Interception>() |> ignore

let showregistrations (container:UnityContainer) = 
    container.Registrations |> Seq.iter (fun i -> printfn "Regist Type:%A" i.RegisteredType)

let testSubstitution (t:TenantStore) = 
    t.NewMethod()

using(new UnityContainer())(fun ctner->
    ctner.AddNewExtension<Interception>() |> ignore
    ctner.RegisterType<ITenantStore, TenantStore>(new Interceptor<TransparentProxyInterceptor>(),
                                                  new InterceptionBehavior<LogingInterceptionBehavior>()) |> ignore
    showregistrations ctner
    let t = ctner.Resolve<ITenantStore>()
    t.Msg()
    let o = (box t) :?> IDisposable
    o.Dispose()
    
    )
    

using(new UnityContainer())(fun ctner->
    ctner.AddNewExtension<Interception>() |> ignore
    ctner.RegisterType<ITenantStore, TenantStore>(new Interceptor<VirtualMethodInterceptor>(),
                                                  new InterceptionBehavior<LogingInterceptionBehavior>()) |> ignore
    ctner.RegisterType<testClass>(new Interceptor<VirtualMethodInterceptor>(),
                                  new InterceptionBehavior<LogingInterceptionBehavior>()) |> ignore
    showregistrations ctner
    let t = ctner.Resolve<ITenantStore>()
    t.Msg()
    let o = (box t) :?> IDisposable
    o.Dispose()
    )


using(new UnityContainer())(fun ctner->
    ctner.AddNewExtension<Interception>() |> ignore
    ctner.RegisterType<testClass>(new Interceptor<VirtualMethodInterceptor>(),
                                  new InterceptionBehavior<LogingInterceptionBehavior>()) |> ignore
    showregistrations ctner
    let t = ctner.Resolve<testClass>()
    t.Test()
    )


using(new UnityContainer())(fun ctner->
    ctner.AddNewExtension<Interception>() |> ignore
    ctner.RegisterType<ITenantStore, TenantStore>(new Interceptor<InterfaceInterceptor>(),
                                                  new InterceptionBehavior<LogingInterceptionBehavior>()) |> ignore
    showregistrations ctner
    let t = ctner.Resolve<ITenantStore>()
    t.Msg()
    )


//container.Teardown(typeof<ITenantStore>)




let testGenericRegistration = 
    container.RegisterType(typedefof<IMessage<_>>, typedefof<Message<_>>) |> ignore

    let y = container.Resolve<IMessage<int>>()                                              
    y.Send(413)                                     
    let z = container.Resolve<IMessage<bool>>()                                              
    z.Send(true)                                     



let testResolve = 
    using(new TenantStore())(fun x -> 
        (x:>ITenantStore).Msg())

    container.RegisterType<ITenantStore, TenantStore>(
        new ContainerControlledLifetimeManager()
        ).RegisterType<ManagementController>(
        new InjectionConstructor(typeof<ITenantStore>, typeof<string>)) |> ignore

    using(container.Resolve<ManagementController>(new ParameterOverride("_name", (box "hello"))))(fun myctrl ->
        printfn "%A" myctrl.TStore)


//new ResolvedParameter

let testPerResolveLifetimeManager() = 
    let o = container.Resolve<CombineManagement>()
    printfn "%d,%d" (o.M1.TStore.GetHashCode()) (o.M2.TStore.GetHashCode())
    let a = container.Resolve<ManagementController>()
    let b = container.Resolve<ExManagementController>()
    let c = container.Resolve<ManagementController>()
    let d = container.Resolve<ExManagementController>()
    printfn "%A:%A\n%A:%A" (a.TStore.GetHashCode()) (b.TStore.GetHashCode()) (c.TStore.GetHashCode()) (d.TStore.GetHashCode())


type MyClass() as x= 
    [<Dependency>]
    member val PProperty: ITenantStore option = None with get, set

    [<DefaultValue>]
    val mutable t:ITenantStore
        let t = new TenantStore() :> ITenantStore

let testPropertyInterception = 
    container.RegisterType<ITenantStore, TenantStore>().RegisterType<MyClass>() |> ignore

    let x = container.Resolve<MyClass>()
    printfn "%A,%A" (x.PProperty.Value.GetHashCode()) (x.PProperty.Value.GetType())

AllClasses.FromLoadedAssemblies() |> Seq.iter (fun i -> printfn "%s" (i.FullName))



open System.Threading
open System
open System

[<EntryPoint>]
let main argv = 
    printfn "%A" argv
    0 // 返回整数退出代码

open System.Collections
open System.Collections.Generic

let rec factorial(x) = 
    printfn "factorial(%d)" x
    if(x <= 0) then 
        1
    else
        x*factorial(x-1)
    
let memorize(f) =
    let cache = new Dictionary<_, _>()
    (fun x -> 
        match cache.TryGetValue(x) with
        |true, v -> v
        |_     -> 
            let v = f(x)       //this one should be memorized,too
            cache.Add(x, v)
            v)

let facMem = memorize factorial

let rec factorial = memorize(fun (x:int64) -> 
    printfn "factorial(%d)" x
    if(x <= 0L) then 1L else x * factorial(x-1L))

let factorial x:bigint = 
    let rec factorialutil (x:bigint) (result:bigint) = 
        if (x <= 0I) then result
        else factorialutil (x-1I) (x * result)
    factorialutil x 1I

let x = 1L

let init(f) = f()


factorial 30
factorial 100000


facMem 1

facMem 4