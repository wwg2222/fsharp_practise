﻿module ExceptionHandlingPractise

#if COMPILED
#else
#r "E:/WorkHell/fsharp-practise/packages/EnterpriseLibrary.Data.6.0.1304.0/lib/NET45/Microsoft.Practices.EnterpriseLibrary.Data.dll"
#r "E:/WorkHell/fsharp-practise/packages/EnterpriseLibrary.Common.6.0.1304.0/lib/NET45/Microsoft.Practices.EnterpriseLibrary.Common.dll"
#r "E:/WorkHell/fsharp-practise/packages/EnterpriseLibrary.ExceptionHandling.6.0.1304.0/lib/NET45/Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.dll"
#r "E:/WorkHell/fsharp-practise/packages/EnterpriseLibrary.ExceptionHandling.Logging.6.0.1304.0/lib/NET45/Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging.dll"
#r "E:/WorkHell/fsharp-practise/packages/EnterpriseLibrary.Logging.6.0.1304.0/lib/NET45/Microsoft.Practices.EnterpriseLibrary.Logging.dll"
#r "System"
#r "System.Data"
#r "System.Configuration"
#r "System.ServiceModel"
#endif


open System
open System.Configuration
open Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
open Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging
open Microsoft.Practices.EnterpriseLibrary.Logging
open Microsoft.Practices.EnterpriseLibrary.Logging.Formatters
open Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners
open System.Diagnostics

//make logger
//format
let formatter = new TextFormatter("TimeStamp: {timestamp}{newline}Message:{message}{newline}Category:{category}{newline}Priority:{priority}{newline}EventID:{eventid}{newline}Severity:{severity}{newline}Title:{title}{newline}Machine:{machine}{newline}App Domain:{localAppDomain}{newline}ProcessID:{localProcessId}{newline}Process Name:{localProcessName}{newline}Thread Name:{threadName}{newline}Win32 ThreadID:{win32Thread}{newline}Extended Properties:{dictinary({key}-{value}{newline})}")
//listener
let flatFileTraceListener = new FlatFileTraceListener(@"c:\Temp\rigid.log", "------------------------------", "------------------------------",formatter)
let eventlog = new EventLog("Application", ".", "Enterprise Libray Logging")
let eventlogTraceListener = new FormattedEventLogTraceListener(eventlog)
//configuration
let config = new LoggingConfiguration()
config.AddLogSource("General", SourceLevels.All, true, [|flatFileTraceListener :> TraceListener|]) |> ignore
//config.LogSources.Clear()config.AddLogSource("General", SourceLevels.All, true).AddTraceListener(flatFileTraceListener)
//Special sources configuration
//config.SpecialSources.LoggingErrorsAndWarnings.AddTraceListener
//config.LogSources.["General"].AddTraceListener(flatFileTraceListener)
let logWriter = new LogWriter(config)


let exceptionShielding = [new ExceptionPolicyEntry(typeof<Exception>,
                                                        PostHandlingAction.ThrowNewException,
                                                        [|new WrapHandler("Application Error. Please contact rigid", typeof<Exception>)|])
                         ]

let replacingException = [new ExceptionPolicyEntry(typeof<Exception>,
                                                        PostHandlingAction.ThrowNewException,
                                                        [|new ReplaceHandler("Application Error. Please contact rigid", typeof<Exception>)|])
                         ]
let loggingAndReplacing = [new ExceptionPolicyEntry(typeof<Exception>,
                                                        PostHandlingAction.NotifyRethrow,
                                                        [|new LoggingExceptionHandler("General", 1000, TraceEventType.Error, "Rigid Service", 5, typeof<TextExceptionFormatter>, logWriter);
                                                          new ReplaceHandler("Application Error. Please contact rigid", typeof<Exception>)|])
                         ]


let policies = [new ExceptionPolicyDefinition("Wrap Exception", exceptionShielding);
                new ExceptionPolicyDefinition("Replace Exception", replacingException);
                new ExceptionPolicyDefinition("Log and Replace Exception", loggingAndReplacing)]

let exceptionManager = new ExceptionManager(policies)
ExceptionPolicy.SetExceptionManager(exceptionManager)

//most simple example
exceptionManager.Process((fun () -> 1/0 ), "Log and Replace Exception")


 
type SalaryCalculator() = 
    member this.GetWeeklySalary (employeeID:string) (weeks:int) =            

        let salary = 1000
        try
            salary/weeks
        with
        | ex -> let template = "Error calculating salary for {0}. " +
                               "Salary: {1}. Weeks: {2}\n" +
                               "Connection: {3}\n" +
                               "{4}"
                let informationException = new Exception(String.Format(template, "John", salary, weeks, "Connecting string", ex.Message))
                raise informationException


let a = new SalaryCalculator()

a.GetWeeklySalary "123" 0
exceptionManager.Process((fun () -> a.GetWeeklySalary "123" 0), "Wrap Exception")