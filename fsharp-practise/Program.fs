﻿// 在 http://fsharp.net 上了解有关 F# 的更多信息
// 请参阅“F# 教程”项目以获取更多帮助。

module test


open System
open System.Windows.Forms
open System.Drawing
open System


let form = new Form(Width = 400, Height = 300, Visible = true, Text = "Bid")
form.TopMost <- true

let whoelpanle = new FlowLayoutPanel()
form.Controls.Add(whoelpanle)
whoelpanle.FlowDirection <- FlowDirection.TopDown
whoelpanle.Width <- 400
whoelpanle.Controls.Add(new Button())

let Addpanel (ppanle:FlowLayoutPanel) =                                                          
    whoelpanle.Controls.Add(new Button())
    let panel = new FlowLayoutPanel()
    panel.FlowDirection <- FlowDirection.LeftToRight
    panel.Width <- 400
    let slidebar = new TrackBar()
    slidebar.Minimum <- 0
    slidebar.Maximum <- 100
    slidebar.Width <- 200
    slidebar.Scroll.Add(fun _ -> textbox.Text <- slidebar.Value.ToString())
    let checkbox = new CheckBox()
    checkbox.Width <- 30
    let textbox = new TextBox()
    textbox.Width <- 60
    panel.Controls.Add(textbox)
    panel.Controls.Add(checkbox)
    panel.Controls.Add(slidebar)
    panel.Dock <- DockStyle.Fill

    ppanle.Controls.Add(panel)
    slidebar, checkbox, textbox

Addpanel whoelpanle






slidebar.Value
let ateamwin = 1.37
let draw = 3.90
let ateamfail = 7.25

let totalmoney = 100.0

let bid odds x = 
    odds * x

let bidx x = 
    bid ateamwin x

let bidxy x = 
    bid draw x

let bidy x = 
    bid ateamfail x

(bidx 60.0) - 100.0

(bidxy 20.0) - 100.0

(bidy 20.0) - 100.0


60.0 * 0.37

type IntTree = 
    | Leaf of int
    | Node of IntTree*IntTree

let tree  = Node(Node(Node(Leaf(5), Leaf(8)), Leaf(2)),Node(Leaf(3), Leaf(9)))

let rec sumTree tree = 
    match tree with
    |Leaf(n)  -> n
    |Node(l,r)-> sumTree(l)+sumTree(r)

sumTree(tree)

open System
let rnd = new Random()
let numbers = List.init 100000 (fun x -> rnd.Next(-50, 51))

let iTree = 
    numbers |> List.fold (fun seed num -> Node(Leaf(num),seed)) (Leaf(0))

let rec sumTreec tree cons = 
    match tree with
    |Leaf(n) -> cons(n)
    |Node(l, r) -> sumTreec l (fun lsum -> 
                       sumTreec r (fun rsum ->
                           cons(lsum + rsum)))

sumTreec iTree (fun x -> printfn "%d" x)



[<EntryPoint>]
let main argv = 
    printfn "%A" argv
    0 // 返回整数退出代码
