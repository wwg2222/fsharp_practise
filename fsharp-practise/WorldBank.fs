﻿module WorldBank

#if INTERACTIVE
#r "System.Web"
#r "System.Net"
#r @"E:\WorkHell\fsharp-practise\packages\FSharpx.Core.1.8.41\lib\40\FSharpx.Core.dll"
#endif

open System.Web
open System.IO
open System.Net
open FSharpx


let downloadurl (url:string) = async{
    let req = WebRequest.Create(url)
    use! resp = req.AsyncGetResponse()
    let  stream = resp.GetResponseStream() 
    use reader = new StreamReader(stream) 
    return! reader.FSharpAsyncReadToEnd()
}

let worldbankurl functions props = 
    seq{
        yield "http://api.worldbank.org"
        for item in functions do
            yield "/" + HttpUtility.UrlEncode(item:string)
        yield "?per_page=1000"
        for key, value in props do
            yield "&" + key + "=" + HttpUtility.UrlEncode(value:string)
    } |> String.concat ""

let url = worldbankurl ["countries"] [("region","WLD")]

HttpUtility.HtmlDecode(Async.RunSynchronously (downloadurl url))
